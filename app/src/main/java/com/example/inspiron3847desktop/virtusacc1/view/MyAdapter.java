package com.example.inspiron3847desktop.virtusacc1.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.inspiron3847desktop.virtusacc1.R;
import com.example.inspiron3847desktop.virtusacc1.model.Response;

import java.util.Date;
import java.util.List;

/**
 * Created by Inspiron3847Desktop on 1/3/2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private List<Response> passList;
    private Context context;

    public MyAdapter(Context c){
        context = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Response pass = passList.get(position);
        Date date = new Date(pass.getRisetime()*1000);
        String duration = pass.getDuration().toString();
        String message = context.getString(R.string.pass_string, date.toString(), duration);
        holder.textView.setText(message);
    }

    @Override
    public int getItemCount() {
        if(passList==null){
            return 0;
        } else {
            return passList.size();
        }
    }

    public void setPassList(List<Response> list){
        passList = list;
    }

    public List<Response> getPassList(){
        return passList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.itemTextView);
        }
    }


}
