
package com.example.inspiron3847desktop.virtusacc1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("duration")
    @Expose
    private Long duration;
    @SerializedName("risetime")
    @Expose
    private Long risetime;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Response() {
    }

    /**
     * 
     * @param duration
     * @param risetime
     */
    public Response(Long duration, Long risetime) {
        super();
        this.duration = duration;
        this.risetime = risetime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getRisetime() {
        return risetime;
    }

    public void setRisetime(Long risetime) {
        this.risetime = risetime;
    }

    @Override
    public String toString() {
        return "Duration: " + getDuration();
    }
}
