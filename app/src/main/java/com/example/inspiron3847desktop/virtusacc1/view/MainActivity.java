package com.example.inspiron3847desktop.virtusacc1.view;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.inspiron3847desktop.virtusacc1.R;
import com.example.inspiron3847desktop.virtusacc1.model.Response;
import com.example.inspiron3847desktop.virtusacc1.presenter.Presenter;

import java.util.List;


public class MainActivity extends AppCompatActivity implements IView {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private MyAdapter myAdapter;

    private Presenter presenter;
    private static final int MY_PERMISSIONS_LOCATION = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create RecyclerView and adapter
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        myAdapter = new MyAdapter(this);
        recyclerView.setAdapter(myAdapter);

        //Given more time, would have implemented code to handle configuration changes
        presenter = new Presenter(this);
    }



    public void findPasses(View view) {
        presenter.getPasses();
    }


    @Override
    public void setItems(List<Response> list) {
        myAdapter.setPassList(list);
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void quitView() {
        finish();
    }

    @Override
    public Activity getActivity() {
        return this;
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSIONS_LOCATION: {
                if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    //if permission granted, continue with normal flow
                    presenter.getLocation();
                } else{
                    //if permission not granted, quit activity
                    makeToast("App cannot function without granting requested permissions");
                    quitView();
                }
            }
        }
    }
}
