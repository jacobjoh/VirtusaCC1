package com.example.inspiron3847desktop.virtusacc1.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.inspiron3847desktop.virtusacc1.model.MyEndPoint;
import com.example.inspiron3847desktop.virtusacc1.model.Passes;
import com.example.inspiron3847desktop.virtusacc1.view.IView;
import com.example.inspiron3847desktop.virtusacc1.view.MainActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Inspiron3847Desktop on 1/4/2018.
 */

public class Presenter {

    private IView view;

    private static final String TAG = Presenter.class.getName();
    private static final String BASE_URL = "http://api.open-notify.org";
    private static final int MY_PERMISSIONS_LOCATION = 1;

    private FusedLocationProviderClient locationProviderClient;
    private MyEndPoint myAPI;

    private LocationCallback locationCallback;

    public Presenter(IView v){
        view = v;

        //initialize location client
        locationProviderClient = LocationServices.getFusedLocationProviderClient((MainActivity) view);

        //create retrofit instance
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        //create MyEndPoint instance
        myAPI = retrofit.create(MyEndPoint.class);



    }

    @TargetApi(23)
    public void getPasses(){
        //check if app has correct permissions
        if (ActivityCompat.checkSelfPermission(view.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // No need to show an explanation of why this app needs location as it is obvious, we can go ahead and request the permission.

            view.getActivity().requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_LOCATION);

        } else {
            getLocation();
        }
    }


    @SuppressLint("MissingPermission")
    public void getLocation(){
        //get most recent location
        locationProviderClient.getLastLocation().addOnSuccessListener(view.getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                makeCall(location);
            }
        });

        //create a LocationRequest to receive automatic updates
        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location location = locationResult.getLastLocation();
                makeCall(location);
            }
        };

        //request updates
        locationProviderClient.requestLocationUpdates(locationRequest,
                locationCallback,
                null /* Looper */);

    }






    public void makeCall(Location location) {
        if(location==null){
            throw new NullPointerException("null location");
        }
        //get latitude and longitude
        Double latitude = location.getLatitude();
        Double longitude = location.getLongitude();

        //make API call on separate thread
        Observable<Passes> call = myAPI.getPasses(latitude,longitude);

        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Passes>() {
                    @Override
                    public void onNext(Passes value) {
                        view.setItems(value.getResponse());
                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        String exceptionMessage = e.getMessage();

                        Log.d(TAG, exceptionMessage);
                        view.makeToast(exceptionMessage);
                        view.setItems(null);
                    }

                });
    }


}
