package com.example.inspiron3847desktop.virtusacc1.view;

import android.app.Activity;

import com.example.inspiron3847desktop.virtusacc1.model.Response;

import java.util.List;

/**
 * Created by Inspiron3847Desktop on 1/4/2018.
 */

public interface IView {

    void setItems(List<Response> list);

    void makeToast(String message);

    void quitView();

    Activity getActivity();

}
