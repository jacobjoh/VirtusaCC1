package com.example.inspiron3847desktop.virtusacc1.model;

import com.example.inspiron3847desktop.virtusacc1.model.Passes;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Inspiron3847Desktop on 1/3/2018.
 */

public interface MyEndPoint {

    @GET("iss-pass.json")
    Observable<Passes> getPasses(@Query("lat") Double latitude, @Query("lon") Double longitude);

}
